const Product = require("../models/Product");


// ########### Product Creation ########### 
module.exports.addProduct = (data) => {
	if (data.isAdmin) {
		let newProduct = new Product({
		name : data.product.name,
		description : data.product.description,
		price : data.product.price
	});
		return newProduct.save().then((product, error) => {
			if (error) {
				return false;
			} else {
				return true;
			};
		});
	};
	let message = Promise.resolve('User must be ADMIN to add new product!');
	return message.then((value) => {
		return {value};
	});
};
// ########### Product Creation ########### END *********

// ########### Retrieve all products ########### 
module.exports.getAllProducts = () => {
  return Product.find().then(result => {
    return result;
  });
};
// ########### Retrieve all products ########### END *********

// ########### Retrieve all active products ########### 
module.exports.getAllActiveProduct = () => {
	return Product.find({isActive : true}).then(result => {
		return result;
	});
};
// ########### Retrieve all active products ########### END *********


// ########### Retrieve Single product ########### 
module.exports.getSingleProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		return result;
	});
};
// ########### Retrieve Single product ########### END *********


//  ########### Update product (admin only) ########### 
module.exports.updateProduct = (data) => {
	if (data.isAdmin) {
		let updatedProduct = {
		name : data.product.name,
		description : data.product.description,
		price : data.product.price
		};
			return Product.findByIdAndUpdate(data.productId, updatedProduct).then((product, error) => {
				if (error) {
					return false;
				} else {
					return true;
				};
			});
	};
	let message = Promise.resolve('User must be ADMIN to update product!');
	return message.then((value) => {
		return {value};
	});
}
//  ########### Update product (admin only) ########### END *********


//  ########### Archive product (admin only) ########### 
module.exports.archiveProduct = async (data) => {
  if (data.isAdmin) {
    const product = await Product.findById(data.productId);
    if (!product) {
      return false;
    }
    product.isActive = !product.isActive;
    await product.save();
    return true;
  } else {
    return { message: 'User must be ADMIN to archive product!' };
  }
};

//  ########### Archive product (admin only) ########### END *********
