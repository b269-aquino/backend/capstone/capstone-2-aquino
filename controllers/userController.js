const User = require("../models/User");

const Product = require("../models/Product");

const bcrypt = require("bcrypt");

const auth = require("../auth");

// ########### User Registration ########### 

module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		email: reqBody.email,
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		mobileNo: reqBody.mobileNo,
		password: bcrypt.hashSync(reqBody.password, 10)
	});
	return newUser.save().then((user, error) =>{
		if (error) {
			return false;
		} else {
			return true;
		}
	});
};
// ########### User Registration ########### END *********


// ########### User Login for Token ########### 
module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if (result == null){
			return false
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
			if (isPasswordCorrect) {
				return {access: auth.createAccessToken(result)}
			} else {
				return false;
			};
		};
	});
};
// ########### User Login ########### END *******`**


//  ########### User Checkout ########### 
module.exports.checkout = async (data) => {
	let isUserUpdated = await User.findById(data.userId).then(user => {
		user.orders.push({productId: data.productId});
		return user.save().then((user, error) => {
			if (error){
				return false;
			} else {
				return true;
			};
		});
	});

	let isProductUpdated = await Product.findById(data.productId).then(product => {product.orders.push({userId: data.userId});
		return product.save().then((product, error) => {
			if (error) {
				return false;
			} else {
				return true;
			};
		});
	});

	if(isUserUpdated && isProductUpdated){
		return true;
	} else {
		return false;
	};
};
//  ########### User Checkout ###########  END *********

// ########### Set User as Admin (Admin Only) ########### 
module.exports.setUserAsAdmin = (data) => {
	if (data.isAdmin) {
		let userUpdate = {
			isAdmin: true
		};
		return User.findByIdAndUpdate(data.userId, userUpdate).then((user, error) => {
			if (error) {
				return false;
			} else {
				return true;
			}
		});
	}
	let message = Promise.resolve('Only ADMIN can set and user as admin!');
	return message.then((value) => {
		return {value};
	});
}
// ########### Set User as Admin (Admin Only) ########### END *********

// ########### Retrieve user details ###########
module.exports.getUser = (data) => {
	return User.findById(data.userId).then(result => {
		result.password = "";
		return result;
	});
};
// ########### Retrieve user details ########### END *********

// ADD-ONS

module.exports.checkEmailExists = (reqBody) => {
	// The result is sent back to the Postman via the "then" method found in the route file
	return User.find({email : reqBody.email}).then(result => {
		// The "find" method returns a record if a match is found
		if (result.length > 0) {
			return true;
		// No duplicate email found
		// The user is not yet registered in the database
		} else {
			return false;
		};
	});
};