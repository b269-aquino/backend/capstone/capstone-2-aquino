const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");

const auth = require("../auth");

router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
})

router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
});

router.post("/checkout", auth.verify, (req, res) => {
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		productId: req.body.productId
	}

	userController.checkout(data).then(resultFromController => res.send(resultFromController));
});


router.patch("/:userId/setAdmin", auth.verify, (req,res) => {

	const data = {
		userId : req.params.userId,
		user : req.body,
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}

	userController.setUserAsAdmin(data).then(resultFromController => res.send(resultFromController));
});

router.get("/details", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	userController.getUser({userId : userData.id}).then(resultFromController => res.send(resultFromController));
});

// ADD-ONS

router.post("/checkEmail", (req,res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
})


// ########### ACCESS TO index.js ###############
module.exports = router;