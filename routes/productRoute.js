const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController");

const auth = require("../auth");

router.post("/add", auth.verify, (req, res) => {

	const data = {
		product : req.body,
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}

	productController.addProduct(data).then(resultFromController => res.send(resultFromController));
});

router.get("/allProducts", (req, res) => {
	productController.getAllProducts().then(resultFromController => res.send(resultFromController));
});

router.get("/active", (req, res) => {
	productController.getAllActiveProduct().then(resultFromController => res.send(resultFromController));
});

router.get("/:productId", (req, res) => {
	productController.getSingleProduct(req.params).then(resultFromController => res.send(resultFromController));
});

router.put("/:productId/update", auth.verify, (req,res) => {

	const data = {
		productId : req.params.productId,
		product : req.body,
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}

	productController.updateProduct(data).then(resultFromController => res.send(resultFromController));
});

router.patch("/:productId/archive", auth.verify, (req,res) => {

	const data = {
		productId : req.params.productId,
		product : req.body,
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}

	productController.archiveProduct(data).then(resultFromController => res.send(resultFromController));
});

module.exports = router;