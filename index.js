// ################## REQUIRE ####################

const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

const userRoute = require("./routes/userRoute");
const productRoute = require("./routes/productRoute");
const orderRoute = require("./routes/orderRoute");
const app = express();

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// ################## MONGOOSE CONNECTION ####################
mongoose.connect("mongodb+srv://kchristianaquino:admin123@zuitt-bootcamp.jthn0f5.mongodb.net/eCommerceAPI?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);
mongoose.connection.once("open", () => console.log(`Connected to Database!`));


app.use("/users", userRoute);
app.use("/products", productRoute);
app.use("/orders", orderRoute); 
// ################## SERVER LISTENER ####################
app.listen(process.env.PORT || 4000, () => console.log(`Now connected to port ${process.env.PORT || 4000} !`));